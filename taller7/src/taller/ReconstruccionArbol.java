package taller;

import java.io.*;
import java.util.*;

import org.omg.CORBA_2_3.portable.InputStream;


public class ReconstruccionArbol {
	
	public final static String ARCHIVO = "./data/ejemplo.properties";
	private String[] preorden;
	private String[] inorden;
	
	public void  cargarArchivo(String nombre) throws Exception{
		Properties prop = new Properties();
		FileInputStream input = new FileInputStream(nombre);
		prop.load(input);
		preorden = prop.getProperty("preorden").split(",");
		inorden = prop.getProperty("inorden").split(",");		
	}

}
