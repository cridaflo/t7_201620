package taller;

public class ArbolSimple<T> {
	
	class Nodo<T>{
		private Nodo padre;
		private Nodo izq;
		private Nodo der;
		private T data;
		
		public Nodo(T pData){
			data = pData;
		}
		public boolean equals(Nodo elOtro) {
			return data.equals(elOtro.data);
		}
		
		public void bucarNodos(T pData, Lista<Nodo<T>> rta){
			if(data.equals(pData)){
				rta.agregarAlPrincipio(this);
			}
			if(izq != null){
				izq.bucarNodos(pData, rta);
			}
			if(der!=null){
				der.bucarNodos(pData, rta);
			}
					
		}
		
		public void agregarDerecho(T pData){
			der = new Nodo<T>(pData);
		}
		public void agregarIzquierdo(T pData){
			der = new Nodo<T>(pData);
		}
	
	}
	
	private Nodo<T> raiz;
	
	public ArbolSimple(Nodo<T> pRaiz){
		raiz=pRaiz;
	}
	public ArbolSimple(){
		raiz=null;
	}
	
	public Nodo<T> bucarNodos(T pData){
		Lista<Nodo<T>> rta = new Lista<Nodo<T>>();
		raiz.bucarNodos(pData, rta);
		return rta;
	}

}
